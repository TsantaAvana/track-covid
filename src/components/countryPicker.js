import React, {useState, useEffect} from "react";
import {NativeSelect, FormControl} from '@material-ui/core';
import styled from 'styled-components';
import { fetchCountries } from '../api/index';
import {device} from '../style/device';
import {withStyles} from '@material-ui/core/styles'



const StyledFromControl = styled(FormControl)`
  width : 30%;
  margin-bottom: 30px !important;
  ${device.tablet`
     width: 80%;
   `};
`;

const StyledSelect = withStyles({
  root: {
    borderRadius: 5
  }
})(NativeSelect);





export const CountryPicker = ({ handleCountryChange }) => {

  const [countries, setCountries] = useState([]);

  useEffect(()=> {
    const fetchCountryAPI = async () => {
      setCountries(await fetchCountries());
    }

    fetchCountryAPI();
  },[]);
  
  
  return (
    <>
      <StyledFromControl>
        <StyledSelect defaultValue="" onChange={(e)=>handleCountryChange(e.target.value)}>
          <option value=""> Global</option>
          {countries.map((country,i) => <option key={i} value={country}> { country }</option>)}
        </StyledSelect>
      </StyledFromControl>
    </>
  );
};

export default CountryPicker;
